<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:12:25
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\base\component\Shopware.window.SimpleModule.js" */ ?>
<?php /*%%SmartyHeaderCode:241065981f9e94e20f2-99654976%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '06d8230427a678e59f2839dae92dbe85229c629e' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\base\\component\\Shopware.window.SimpleModule.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '241065981f9e94e20f2-99654976',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981f9e94f83c8_38953957',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981f9e94f83c8_38953957')) {function content_5981f9e94f83c8_38953957($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */
Ext.define('Shopware.window.SimpleModule', {
    extend: 'Enlight.app.Window',
    height: '90%',
    width: '80%',
    layout: 'fit'
});<?php }} ?>