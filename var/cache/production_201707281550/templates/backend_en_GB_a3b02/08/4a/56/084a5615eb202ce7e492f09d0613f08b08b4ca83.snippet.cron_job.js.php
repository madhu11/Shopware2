<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:24
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\store\form\cron_job.js" */ ?>
<?php /*%%SmartyHeaderCode:139725981fbc8de40b7-65635151%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '084a5615eb202ce7e492f09d0613f08b08b4ca83' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\store\\form\\cron_job.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '139725981fbc8de40b7-65635151',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc8e05334_76784623',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc8e05334_76784623')) {function content_5981fbc8e05334_76784623($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//
Ext.define('Shopware.apps.Config.store.form.CronJob', {
    extend: 'Ext.data.Store',
    model: 'Shopware.apps.Config.model.form.CronJob',
    remoteSort: true,
    remoteFilter: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        url: '<?php echo '/Projects/Generation4/Shopware/backend/Config/getTableList';?>?_repositoryClass=cronJob',
        api: {
            create: '<?php echo '/Projects/Generation4/Shopware/backend/Config/saveTableValues';?>?_repositoryClass=cronJob',
            update: '<?php echo '/Projects/Generation4/Shopware/backend/Config/saveTableValues';?>?_repositoryClass=cronJob',
            destroy: '<?php echo '/Projects/Generation4/Shopware/backend/Config/deleteTableValues';?>?_repositoryClass=cronJob'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>