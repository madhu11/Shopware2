<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:05
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\element\custom_sorting_grid.js" */ ?>
<?php /*%%SmartyHeaderCode:7835981fbb577f600-07877247%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b10980c6a561ee0f421acce7bc159b9ba43dba3' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\element\\custom_sorting_grid.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7835981fbb577f600-07877247',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbb579cef5_57652635',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbb579cef5_57652635')) {function content_5981fbb579cef5_57652635($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

Ext.define('Shopware.apps.Config.view.element.CustomSortingGrid', {
    extend: 'Shopware.form.field.CustomSortingGrid',
    alias: 'widget.config-element-custom-sorting-grid',

    initComponent: function() {
        var me = this,
            factory = Ext.create('Shopware.attribute.SelectionFactory');

        me.store = factory.createEntitySearchStore("Shopware\\Models\\Search\\CustomSorting");
        me.searchStore = factory.createEntitySearchStore("Shopware\\Models\\Search\\CustomSorting");
        me.callParent(arguments);

        if (me.value) {
            me.setValue(me.value);
        }
    }
});<?php }} ?>