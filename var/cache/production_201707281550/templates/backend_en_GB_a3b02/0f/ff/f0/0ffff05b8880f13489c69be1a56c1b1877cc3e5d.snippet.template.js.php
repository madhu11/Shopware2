<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:27:53
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\category\store\template.js" */ ?>
<?php /*%%SmartyHeaderCode:1987259826e0992a868-81966404%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0ffff05b8880f13489c69be1a56c1b1877cc3e5d' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\category\\store\\template.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1987259826e0992a868-81966404',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826e09949d50_85194104',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826e09949d50_85194104')) {function content_59826e09949d50_85194104($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Category
 * @subpackage Store
 * @version    $Id$
 * @author shopware AG
 */

/*  */

/**
 * Shopware Store - for the Category backend module.
 *
 * The template store loads and stores category template data
 */
//
Ext.define('Shopware.apps.Category.store.Template', {
    /**
     * Parent Object
     * @string
     */
    extend : 'Ext.data.Store',
    /**
     * Store to use
     * @string
     */
    alias : 'store.template',
    /**
     * USe remote filtering
     * @boolean
     */
    remoteFilter: true,
    /**
     * Defines if the store is loaded from the start or not
     * @boolean
     */
    autoLoad : false,
    /**
     * Default page size is 30 items
     * @integer
     */
    pageSize : 30,
    /**
     * Model to use for this store
     * @string
     */
    model : 'Shopware.apps.Category.model.Template',

    /**
     * Proxy config object.
     * @object
     */
    proxy : {
        type : 'ajax',
         /**
         * Configure the url mapping for the different
         * store operations based on
         * @object
         */
        api : {
            read : '<?php echo '/Projects/Generation4/Shopware/backend/category/getTemplateSettings';?>'
        },
        /**
         * Configure the data reader
         * @object
         */
        reader : {
            type : 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>