<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:21:01
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\translation\view\main\form.js" */ ?>
<?php /*%%SmartyHeaderCode:1425459826c6d189f66-91087342%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '117f89a15f12e2a7a2bebe59d5fbe81babd3d82b' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\translation\\view\\main\\form.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1425459826c6d189f66-91087342',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826c6d225ed7_95092433',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826c6d225ed7_95092433')) {function content_59826c6d225ed7_95092433($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Translation
 * @subpackage View
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Translation Manager Main Form
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Translation.view.main.Form',
/** @lends Ext.form.Panel# */
{
    extend: 'Ext.form.Panel',
    alias: 'widget.translation-main-form',
    bodyPadding: 10,
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'form_title','default'=>'Translatable fields','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_title','default'=>'Translatable fields','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Translatable fields<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'form_title','default'=>'Translatable fields','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    layout: 'anchor',

    disabled: true,
    defaultType: 'textfield',
    defaults: {
        labelStyle: 'font-weight: 700; text-align: right;',
        labelWidth: 155,
        anchor: '100%'
    },

    /**
     * Original title of the form panel. This is neccessary
     * due to the fact that the title will be overridden in
     * the controller.
     *
     * @default null
     * @string
     */
    originalTitle: null,
    /**
     * Form elements
     * @array
     */
    items : [],

    /**
     * Initializes the component and builds up the main interface
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this, items = [];

        me.originalTitle = me.title;

        Ext.each(me.translatableFields, function(currentField) {
            var elementType = currentField.xtype || { };
            switch(elementType) {
                case 'codemirrorfield' :
                    currentField.height = "100";
                    break;
            }
            currentField.hidden = false;
            items.push(currentField);
        });
        me.items = items;
        me.callParent(arguments);
    }
});
//
<?php }} ?>