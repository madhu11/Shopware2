<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:04
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\store\form\number.js" */ ?>
<?php /*%%SmartyHeaderCode:218525981fbb40b8421-43168613%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '179bcd2032e2b332b8d9d715d83bea33eaf88634' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\store\\form\\number.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '218525981fbb40b8421-43168613',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbb40dc603_01447757',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbb40dc603_01447757')) {function content_5981fbb40dc603_01447757($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * todo@all: Documentation
 */

//
Ext.define('Shopware.apps.Config.store.form.Number', {
    extend: 'Ext.data.Store',
    model:'Shopware.apps.Config.model.form.Number',
    remoteSort: true,
    remoteFilter: true,
    pageSize: 20,
    proxy: {
        type: 'ajax',
        url: '<?php echo '/Projects/Generation4/Shopware/backend/Config/getList';?>?_repositoryClass=number',
        api: {
            create: '<?php echo '/Projects/Generation4/Shopware/backend/Config/saveValues';?>?_repositoryClass=number',
            update: '<?php echo '/Projects/Generation4/Shopware/backend/Config/saveValues';?>?_repositoryClass=number',
            destroy: '<?php echo '/Projects/Generation4/Shopware/backend/Config/deleteValues';?>?_repositoryClass=number'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>