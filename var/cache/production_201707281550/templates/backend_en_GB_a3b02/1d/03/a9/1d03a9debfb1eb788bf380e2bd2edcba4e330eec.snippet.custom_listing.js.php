<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:27:53
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\category\view\category\tabs\custom_listing.js" */ ?>
<?php /*%%SmartyHeaderCode:1442959826e0957b544-48753211%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1d03a9debfb1eb788bf380e2bd2edcba4e330eec' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\category\\view\\category\\tabs\\custom_listing.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1442959826e0957b544-48753211',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826e0962c220_17945027',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826e0962c220_17945027')) {function content_59826e0962c220_17945027($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Category
 * @subpackage Controller
 * @version    $Id$
 * @author shopware AG
 */

//

//

Ext.define('Shopware.apps.Category.view.category.tabs.CustomListing', {
    extend: 'Ext.form.Panel',
    alias: 'widget.category-tab-custom-listing',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/custom_listing_title",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/custom_listing_title",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filter / Sorting<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/custom_listing_title",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    bodyPadding: 20,
    layout: 'anchor',
    name: 'custom-listing',
    cls: 'shopware-form',
    border: 0,
    autoScroll: true,
    mixins: {
        factory: 'Shopware.attribute.SelectionFactory'
    },

    initComponent: function() {
        var me = this;

        me.items = me.createItems();
        me.callParent(arguments);
    },

    createItems: function() {
        var me = this;

        me.sortingFieldSet = Ext.create('Ext.form.FieldSet', {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/sorting_title",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/sorting_title",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Custom sorting<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/sorting_title",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            anchor: '100%',
            items: [
                me.createHideSortingItem(),
                me.createActivateSortingItem(),
                me.createSortingSelection(),
                me.createCopySettingsButton(me.copySortingSettings)
            ]
        });

        me.facetFieldSet = Ext.create('Ext.form.FieldSet', {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/facet_title",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/facet_title",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Custom filter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/facet_title",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            anchor: '100%',
            items: [
                me.createHideFacetItem(),
                me.createActivateFacetItem(),
                me.createFacetSelection(),
                me.createCopySettingsButton(me.copyFacetSettings)
            ]
        });

        return [me.sortingFieldSet, me.facetFieldSet];
    },


    createHideFacetItem: function() {
        var me = this;

        me.hideFilterItem = Ext.create('Ext.form.field.Checkbox', {
            labelWidth: 155,
            name: 'hideFilter',
            inputValue: true,
            uncheckedValue: false,
            dataIndex: 'hideFilter',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('namespace'=>'backend/custom_search/translation','name'=>'view'/'settings_default_settings_no_filter_label')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('namespace'=>'backend/custom_search/translation','name'=>'view'/'settings_default_settings_no_filter_label'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hide filters.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('namespace'=>'backend/custom_search/translation','name'=>'view'/'settings_default_settings_no_filter_label'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });
        return me.hideFilterItem;
    },

    createActivateSortingItem: function() {
        var me = this;

        me.activateSorting = Ext.create('Ext.form.field.Checkbox', {
            labelWidth: 155,
            inputValue: true,
            uncheckedValue: false,
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/activate_sorting",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/activate_sorting",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Active<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/activate_sorting",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            listeners: {
                'change': Ext.bind(me.onActivateSorting, me)
            }
        });
        return me.activateSorting;
    },

    createActivateFacetItem: function() {
        var me = this;

        me.activateFacets = Ext.create('Ext.form.field.Checkbox', {
            labelWidth: 155,
            inputValue: true,
            uncheckedValue: false,
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/activate_facets",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/activate_facets",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Active<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/activate_facets",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            listeners: {
                'change': Ext.bind(me.onActivateFacet, me)
            }
        });
        return me.activateFacets;
    },

    createHideSortingItem: function() {
        var me = this;

        me.hideSorting = Ext.create('Ext.form.field.Checkbox', {
            labelWidth: 155,
            name: 'hideSortings',
            inputValue: true,
            uncheckedValue: false,
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/hide_sorting",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/hide_sorting",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Hide sortings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/hide_sorting",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });
        return me.hideSorting;
    },

    createSortingSelection: function() {
        var me = this, store;

        store = me.createEntitySearchStore("Shopware\\Models\\Search\\CustomSorting");
        store.pageSize = 200;

        me.sortingSelection = Ext.create('Shopware.form.field.CustomSortingGrid', {
            labelWidth: 155,
            disabled: true,
            ignoreDisabled: false,
            store: store,
            searchStore: me.createEntitySearchStore("Shopware\\Models\\Search\\CustomSorting"),
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/sorting_selection",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/sorting_selection",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Available sortings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/sorting_selection",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'sortingIds'
        });
        return me.sortingSelection;
    },

    createFacetSelection: function() {
        var me = this, store;

        store = me.createEntitySearchStore("Shopware\\Models\\Search\\CustomFacet");
        store.pageSize = 200;

        me.facetSelection = Ext.create('Shopware.form.field.CustomFacetGrid', {
            labelWidth: 155,
            disabled: true,
            ignoreDisabled: false,
            store: store,
            searchStore: me.createEntitySearchStore("Shopware\\Models\\Search\\CustomFacet"),
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/facet_selection",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/facet_selection",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Available filter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/facet_selection",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            name: 'facetIds'
        });
        return me.facetSelection;
    },

    createCopySettingsButton: function(copyFunction) {
        var me = this;

        me.copySettingsButton = Ext.create('Ext.button.Button', {
            cls: 'primary small',
            margin: '5 0 0 160',
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/copy_settings_button",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/copy_settings_button",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Apply to subcategories<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/copy_settings_button",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler: Ext.bind(copyFunction, me)
        });
        return me.copySettingsButton;
    },

    copySortingSettings: function() {
        var me = this;

        if (!me.category) {
            return;
        }

        me.fireEvent('saveCategory', me.category, function() {
            Ext.Ajax.request({
                url: '<?php echo '/Projects/Generation4/Shopware/backend/CustomSorting/copyCategorySettings';?>',
                method: 'POST',
                params: {
                    categoryId: me.category.get('id')
                },
                success: function(operation, opts) {
                    Shopware.Notification.createGrowlMessage('', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/copy_success",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/copy_success",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
The sorting configuration has been applied to subcategories.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/copy_success",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                }
            });
        });
    },

    copyFacetSettings: function() {
        var me = this;

        if (!me.category) {
            return;
        }

        me.fireEvent('saveCategory', me.category, function() {
            Ext.Ajax.request({
                url: '<?php echo '/Projects/Generation4/Shopware/backend/CustomFacet/copyCategorySettings';?>',
                method: 'POST',
                params: {
                    categoryId: me.category.get('id')
                },
                success: function(operation, opts) {
                    Shopware.Notification.createGrowlMessage('', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"category/copy_success",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/copy_success",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
The sorting configuration has been applied to subcategories.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"category/copy_success",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                }
            });
        });
    },

    onActivateSorting: function(checkbox, active) {
        var me = this;

        if (active) {
            me.sortingSelection.enable();
            return true;
        }

        me.category.set('sortingIds', null);
        me.sortingSelection.disable();
        me.sortingSelection.store.load();
        return true;
    },

    onActivateFacet: function(checkbox, active) {
        var me = this;

        if (active) {
            me.facetSelection.enable();
            return true;
        }

        me.category.set('facetIds', null);
        me.facetSelection.disable();
        me.facetSelection.store.load();
        return true;
    },


    loadCategory: function(category) {
        var me = this, hasSortings, hasFacets;

        me.loadRecord(category);
        me.category = category;
        me.enable();

        hasSortings = (category.get('sortingIds').length > 0);
        me.activateSorting.setValue(hasSortings);

        if (hasSortings) {
            me.sortingSelection.enable();
        } else {
            me.sortingSelection.disable();
            me.sortingSelection.store.load();
        }

        hasFacets = (category.get('facetIds').length > 0);
        me.activateFacets.setValue(hasFacets);

        if (hasFacets) {
            me.facetSelection.enable();
        } else {
            me.facetSelection.disable();
            me.facetSelection.store.load();
        }

        return true;
    }
});
//<?php }} ?>