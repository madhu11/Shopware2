<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:21
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\facet\listing.js" */ ?>
<?php /*%%SmartyHeaderCode:71725981fbc5ae1cd6-11848181%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '204f2c1149d413de0de245c8fe225fa7c06df34d' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\facet\\listing.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '71725981fbc5ae1cd6-11848181',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc5b18a53_78358095',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc5b18a53_78358095')) {function content_5981fbc5b18a53_78358095($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.facet.Listing', {
    extend: 'Shopware.apps.Config.view.custom_search.common.Listing',
    alias: 'widget.config-custom-facet-listing',
    changePositionUrl: '<?php echo '/Projects/Generation4/Shopware/backend/customFacet/changePosition';?>',

    configure: function() {
        return {
            deleteButton: false,
            editColumn: false,
            pagingbar: false,
            addButton: false,
            displayProgressOnSingleDelete: false,
            columns: {
                name: {
                    header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"name",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"name",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"name",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    sortable: false
                },
                active: {
                    header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"active",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Active<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    sortable: false
                },
                displayInCategories: {
                    header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Display in all categories<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    sortable: false
                }
            }
        };
    },

    createToolbarItems: function() {
        var me = this,
            items = me.callParent(arguments);

        items = Ext.Array.insert(items, 0, [
            {
                xtype: 'button',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"add_attribute_facet",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"add_attribute_facet",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add attribute filter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"add_attribute_facet",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                iconCls: 'sprite-plus-circle-frame',
                handler: Ext.bind(me.addAttributeFacet, me)
            },
            {
                xtype: 'button',
                text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"add_combined_condition_facet",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"add_combined_condition_facet",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Add combined filter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"add_combined_condition_facet",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                iconCls: 'sprite-plus-circle-frame',
                handler: Ext.bind(me.addCombinedFacet, me)
            }
        ]);

        return items;
    },

    createDeleteColumn: function() {
        var me = this;
        var column = me.callParent(arguments);

        column.getClass = function(value, metadata, record) {
            if (!record.get('deletable')) {
                return 'x-hidden';
            }
        };
        return column;
    },

    addAttributeFacet: function() {
        var me = this;

        me.facetForm.setDisabled(false);

        var facet = {};
        facet['Shopware\\Bundle\\SearchBundle\\Facet\\ProductAttributeFacet'] = {
            label: ''
        };

        me.facetForm.loadFacet(
            Ext.create('Shopware.apps.Base.model.CustomFacet', {
                active: 1,
                displayInCategories: 1,
                deletable: true,
                facet: Ext.JSON.encode(facet)
            })
        );
    },

    addCombinedFacet: function() {
        var me = this;

        me.facetForm.setDisabled(false);

        var facet = {};
        facet['Shopware\\Bundle\\SearchBundle\\Facet\\CombinedConditionFacet'] = {
            label: ''
        };

        me.facetForm.loadFacet(
            Ext.create('Shopware.apps.Base.model.CustomFacet', {
                active: 1,
                deletable: true,
                displayInCategories: 1,
                facet: Ext.JSON.encode(facet)
            })
        );
    },

    onSelectionChange: function(selModel, selection) {
        var me = this;

        if (selection.length <= 0) {
            me.facetForm.setDisabled(true);
            return;
        }
        me.facetForm.loadFacet(selection[0]);
    }
});

//
<?php }} ?>