<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:04
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\element\select.js" */ ?>
<?php /*%%SmartyHeaderCode:231285981fbb49ba935-78358923%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '233d291b76f9f3d71e85f10a3819abc783b21b89' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\element\\select.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '231285981fbb49ba935-78358923',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbb49ede37_11827351',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbb49ede37_11827351')) {function content_5981fbb49ede37_11827351($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Config
 * @subpackage Component
 * @version    $Id$
 * @author shopware AG
 */
Ext.define('Shopware.apps.Config.view.element.Select', {
    extend:'Shopware.apps.Base.view.element.Select',
    alias:[
        'widget.config-element-select',
        'widget.config-element-combo',
        'widget.config-element-combobox',
        'widget.config-element-comboremote'
    ],

    queryMode:'remote',

    initComponent:function () {
        var me = this;

        me.callParent(arguments);
        me.queryCaching = (me.store.$className == 'Ext.data.ArrayStore');
    }
});
<?php }} ?>