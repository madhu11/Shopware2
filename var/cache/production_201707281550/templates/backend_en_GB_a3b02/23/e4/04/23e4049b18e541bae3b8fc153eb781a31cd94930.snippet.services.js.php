<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:21:01
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\translation\view\main\services.js" */ ?>
<?php /*%%SmartyHeaderCode:636459826c6d2fd0e7-01036753%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '23e4049b18e541bae3b8fc153eb781a31cd94930' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\translation\\view\\main\\services.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '636459826c6d2fd0e7-01036753',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826c6d338022_22292410',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826c6d338022_22292410')) {function content_59826c6d338022_22292410($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Translation
 * @subpackage View
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Translation Manager Services Window
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Translation.view.main.Services',
/** @lends Ext.window.Window# */
{
    extend: 'Ext.window.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'services_title','default'=>'Translation services','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'services_title','default'=>'Translation services','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Translation services<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'services_title','default'=>'Translation services','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    alias: 'widget.translation-main-services-window',
    width: 400,
    height: 275,

    /**
     * Initializes the component and builds up the main interface
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.items = [{
            xtype: 'form',
            layout: 'anchor',
            border: false,
            defaults: {
                anchor: '100%',
                labelWidth: 155,
                labelStyle: 'font-weight: 700;'
            },
            bodyPadding: '10 10 10 10',
            items: me.createItems()
        }];
        me.buttons = me.createActionButtons();

        me.callParent(arguments);
    },

    /**
     * Creates the items for the main window.
     *
     * @private
     * @return [array] generated items array
     */
    createItems: function() {
        var me = this;

        return [{
            xtype: 'displayfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'service_name','default'=>'Service','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'service_name','default'=>'Service','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Service<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'service_name','default'=>'Service','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            fieldStyle: 'font-weight: bold',
            value: me.serviceName
        }, {
            xtype: 'displayfield',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'source_language','default'=>'Original language','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'source_language','default'=>'Original language','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Original language<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'source_language','default'=>'Original language','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            value: me.activeLanguage.get('text')
        }, {
            xtype: 'combobox',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'select_field','default'=>'Select field','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'select_field','default'=>'Select field','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Select field<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'select_field','default'=>'Select field','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            queryMode: 'local',
            displayField: 'displayField',
            valueField: 'valueField',
            triggerAction: 'all',
            store: me.fieldStore,
            name: 'translationField'
        }, {
            xtype: 'combobox',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'translate_to','default'=>'Translate into','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'translate_to','default'=>'Translate into','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Translate into<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'translate_to','default'=>'Translate into','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            displayField: 'name',
            valueField: 'id',
            triggerAction: 'all',
            queryMode: 'remote',
            store: me.langStore,
            name: 'language'
        }, {
            xtype: 'container',
            style: 'font-style: italic; color: #999', // todo@stp - remove styling in sw4 phase "decline"
            html: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'note_external_services','default'=>'Please note that external translation services open in a new browser window when you press the start translation button.','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'note_external_services','default'=>'Please note that external translation services open in a new browser window when you press the start translation button.','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Please note that external translation services are opened in a new browser window when you press the button to start translation.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'note_external_services','default'=>'Please note that external translation services open in a new browser window when you press the start translation button.','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        }];
    },

    /**
     * Creates the buttons which will be rendered
     * in an docked toolbar at the bottom of the
     * window
     *
     * @private
     * @return [array] generated buttons array
     */
    createActionButtons: function() {
        var me = this;

        return [{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button'/'cancel','default'=>'Cancel','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'cancel','default'=>'Cancel','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'cancel','default'=>'Cancel','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'secondary',
            handler: function() {
                me.destroy();
            }
        },{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button'/'start_translation','default'=>'Start translation','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'start_translation','default'=>'Start translation','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Start translation<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'start_translation','default'=>'Start translation','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            action: 'translation-main-services-window-translate'
        }]
    }
});
//
<?php }} ?>