<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:23
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\sorting\classes\product_attribute_sorting.js" */ ?>
<?php /*%%SmartyHeaderCode:105385981fbc76b98c7-54126247%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '25df6695b7aa128d80cca897ba6bb0fdf9ffadd9' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\sorting\\classes\\product_attribute_sorting.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '105385981fbc76b98c7-54126247',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc76f2a73_73591292',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc76f2a73_73591292')) {function content_5981fbc76f2a73_73591292($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.sorting.classes.ProductAttributeSorting', {
    extend: 'Shopware.apps.Config.view.custom_search.sorting.classes.SortingInterface',
    mixins: {
        factory: 'Shopware.attribute.SelectionFactory'
    },

    getLabel: function() {
        return '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"product_attribute_sorting",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"product_attribute_sorting",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sort by product free text fields<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"product_attribute_sorting",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';
    },

    supports: function(sortingClass) {
        return (sortingClass.indexOf('Shopware\\Bundle\\SearchBundle\\Sorting\\ProductAttributeSorting') >= 0);
    },

    load: function(sortingClass, parameters, callback) {
        if (!Ext.isFunction(callback)) {
            throw 'Requires provided callback function';
        }
        var record = {
            'class': 'Shopware\\Bundle\\SearchBundle\\Sorting\\ProductAttributeSorting|' + parameters.field,
            'label': parameters.backend_label,
            'parameters': parameters
        };

        callback(record);
    },

    create: function(callback) {
        var me = this;

        if (!Ext.isFunction(callback)) {
            throw 'Requires provided callback function';
        }

        Ext.create('Shopware.apps.Config.view.custom_search.sorting.includes.CreateWindow', {
            title: me.getLabel(),
            height: 180,
            width: 500,
            items: [
                me._createAttributeSelection(),
                { xtype: 'custom-search-direction-combo', labelWidth: 150 }
            ],
            callback: function(values) {
                me._createRecord(values, callback);
            }
        }).show();
    },

    _createAttributeSelection: function() {
        var me = this;

        var store = Ext.create('Ext.data.Store', {
            model: 'Shopware.model.Dynamic',
            proxy: {
                type: 'ajax',
                url: '<?php echo '/Projects/Generation4/Shopware/backend/AttributeData/list';?>',
                reader: Ext.create('Shopware.model.DynamicReader'),
                extraParams: {
                    table: 's_articles_attributes'
                }
            }
        });

        return Ext.create('Shopware.form.field.AttributeSingleSelection', {
            labelWidth: 150,
            name: 'field',
            allowBlank: false,
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"product_attribute_sorting_field",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"product_attribute_sorting_field",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Select free text field<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"product_attribute_sorting_field",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            store: store
        });
    },

    _createRecord: function(parameters, callback) {
        var me = this;

        me._requestLabel(parameters.field, function(label) {
            parameters.backend_label = label;

            callback({
                'class': 'Shopware\\Bundle\\SearchBundle\\Sorting\\ProductAttributeSorting|' + parameters.field,
                'label': label,
                'parameters': parameters
            });
        });
    },

    _requestLabel: function(field, callback) {
        Ext.Ajax.request({
            url: '<?php echo '/Projects/Generation4/Shopware/backend/Attributes/getColumn';?>',
            params: {
                table: 's_articles_attributes',
                columnName: field
            },
            success: function(operation, opts) {
                var response = Ext.decode(operation.responseText);
                var label = '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"product_attribute_sorting_short",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"product_attribute_sorting_short",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Free text field<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"product_attribute_sorting_short",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
:';

                response = response.data;

                if (response.label) {
                    label += ' <b>' + response.label + '</b>';
                } else if (response.columnName) {
                    label += ' <b>' + response.columnName + '</b>';
                }

                if (response.helpText) {
                    label += ' <i>[' + response.helpText + ']</i>';
                }

                callback(label);
            }
        });
    },

    _getLabelOfObject: function(values) {
        if (values.label.length > 0) {
            return values.label + ' - '+ values.columnName;
        }
        return values.columnName;
    }
});

//
<?php }} ?>