<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:05
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\element\product_box_layout_select.js" */ ?>
<?php /*%%SmartyHeaderCode:72805981fbb52a0f64-89139519%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2db3d072e767af54c9f4029585c6134b5a5e36c9' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\element\\product_box_layout_select.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '72805981fbb52a0f64-89139519',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbb52c3300_96445229',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbb52c3300_96445229')) {function content_5981fbb52c3300_96445229($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

Ext.define('Shopware.apps.Config.view.element.ProductBoxLayoutSelect', {
    extend: 'Shopware.apps.Base.view.element.ProductBoxLayoutSelect',
    alias: [
        'widget.config-element-product-box-layout-select'
    ],

    createStore: function() {
        this.store = Ext.create('Shopware.apps.Base.store.ProductBoxLayout', {
            displayExtendLayout: false,
            displayBasicLayout: true,
            displayMinimalLayout: true,
            displayImageLayout: true,
            displayListLayout: true
        });
    }

});
<?php }} ?>