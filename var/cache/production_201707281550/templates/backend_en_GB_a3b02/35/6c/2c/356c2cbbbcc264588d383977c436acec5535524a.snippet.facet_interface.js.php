<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:21
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\facet\classes\facet_interface.js" */ ?>
<?php /*%%SmartyHeaderCode:262915981fbc5d4c983-57677155%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '356c2cbbbcc264588d383977c436acec5535524a' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\facet\\classes\\facet_interface.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '262915981fbc5d4c983-57677155',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc5d6c959_02174386',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc5d6c959_02174386')) {function content_5981fbc5d6c959_02174386($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.facet.classes.FacetInterface', {

    /**
     * @api Defines the facet class which created for the \Shopware\Bundle\SearchBundle\Criteria
     * @returns { string }
     */
    getClass: function() {
        throw 'Unimplemented method.';
    },

    /**
     * @api Defines the constructor parameters for the facet.
     *
     * @returns { Array }
     */
    createItems: function () {
        throw 'Unimplemented method.';
    }
});

//<?php }} ?>