<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:23
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\sorting\includes\create_window.js" */ ?>
<?php /*%%SmartyHeaderCode:7955981fbc777e093-04795301%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40bf0a3075350f4db28b6514584c876d31574fd5' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\sorting\\includes\\create_window.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7955981fbc777e093-04795301',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc77af735_23996964',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc77af735_23996964')) {function content_5981fbc77af735_23996964($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.sorting.includes.CreateWindow', {
    extend: 'Enlight.app.Window',
    height: 150,
    width: 380,
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    maximizable: false,
    minimizable: false,
    modal: true,

    initComponent: function () {
        var me = this;

        if (!Ext.isFunction(me.callback)) {
            throw 'Create window requires a provided callback function';
        }

        me.formPanel = me.createFormPanel();
        me.items = [me.formPanel];

        me.dockedItems = [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: [me.createCancelButton(), '->', me.createSaveButton()]
        }];

        me.callParent(arguments);
    },

    createFormPanel: function() {
        var me = this;

        return Ext.create('Ext.form.Panel', {
            items: me.items,
            flex: 1,
            border: false,
            layout: 'anchor',
            bodyPadding: 20,
            defaults: {
                anchor: '100%'
            }
        });
    },

    createCancelButton: function () {
        var me = this;

        me.cancelButton = Ext.create('Ext.button.Button', {
            cls: 'secondary',
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"cancel_button",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"cancel_button",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"cancel_button",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler: Ext.bind(me.onCancel, me)
        });
    },

    createSaveButton: function () {
        var me = this;

        me.saveButton = Ext.create('Ext.button.Button', {
            cls: 'primary',
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler: Ext.bind(me.onSave, me)
        });

        return me.saveButton;
    },

    onCancel: function () {
        this.destroy();
    },

    onSave: function () {
        var me = this,
            values = me.formPanel.getForm().getValues();

        if (!me.formPanel.getForm().isValid()) {
            return;
        }

        me.callback(values);
        me.destroy();
    }
});

//
<?php }} ?>