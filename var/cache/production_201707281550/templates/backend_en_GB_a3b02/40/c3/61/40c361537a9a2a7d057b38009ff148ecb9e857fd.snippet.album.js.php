<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:14:16
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\media_manager\model\album.js" */ ?>
<?php /*%%SmartyHeaderCode:3131059826ad85572e9-08089805%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '40c361537a9a2a7d057b38009ff148ecb9e857fd' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\media_manager\\model\\album.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3131059826ad85572e9-08089805',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826ad85bdbf1_45135623',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826ad85bdbf1_45135623')) {function content_59826ad85bdbf1_45135623($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage Model
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.MediaManager.model.Album', {
    extend: 'Ext.data.Model',
    fields: [
        //
        'id',
        'text',
        'position',
        'mediaCount',
        'parentId',
        'createThumbnails',
        'thumbnailSize',
        'iconCls',
        'albumID' ,
        'thumbnailHighDpi',
        'thumbnailQuality',
        'thumbnailHighDpiQuality'
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/getAlbums';?>',
            create: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/saveAlbum';?>',
            update: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/saveAlbum';?>',
            destroy: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/removeAlbum/targetField/albums';?>'
        },
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
//
<?php }} ?>