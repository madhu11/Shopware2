<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:21
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\common\listing.js" */ ?>
<?php /*%%SmartyHeaderCode:245995981fbc58fcf23-08584490%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4989f939713b0435182835d1287eb48b09700414' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\common\\listing.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '245995981fbc58fcf23-08584490',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc5a67ff0_14958636',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc5a67ff0_14958636')) {function content_5981fbc5a67ff0_14958636($_smarty_tpl) {?>
/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.common.Listing', {
    extend: 'Shopware.grid.Panel',
    alias: 'widget.config-custom-common-listing',
    changePositionUrl: '',

    createColumns: function() {
        var me = this,
            columns = me.callParent(arguments);

        columns = Ext.Array.insert(columns, 0, [me.createSortingColumn()]);

        me.viewConfig = {
            plugins: {
                ptype: 'gridviewdragdrop',
                ddGroup: me.alias + '-drag-and-drop'
            },
            listeners: {
                'drop': Ext.bind(me.onDrop, me)
            }
        };

        return columns;
    },

    createSortingColumn: function() {
        var me = this;

        return {
            width: 24,
            hideable: false,
            renderer : me.renderSorthandleColumn
        };
    },

    renderSorthandleColumn: function (value, metadata) {
        return '<div style="cursor: n-resize;">&#009868;</div>';
    },

    createSelectionModel: function() {
        var me = this;

        return Ext.create('Ext.selection.RowModel', {
            listeners: {
                selectionchange: function (selModel, selection) {
                    return me.onSelectionChange(selModel, selection);
                }
            }
        });
    },

    onDrop: function(node, data, overModel, dropPosition, eOpts ) {
        var me = this,
            position = 0,
            model = data.records.shift();

        if (dropPosition == 'before') {
            position = overModel.get('position') - 1;
        } else {
            position = overModel.get('position') + 1;
        }

        Ext.Ajax.request({
            url: me.changePositionUrl,
            method: 'POST',
            params: {
                id: model.get('id'),
                position: position
            },
            success: function(operation, opts) {
                me.getStore().load();
            }
        });
    }
});

//
<?php }} ?>