<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:21:01
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\translation\store\language.js" */ ?>
<?php /*%%SmartyHeaderCode:1994159826c6d4173c8-45576346%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4bab1096a64855397af7c3f786b7314d7a43143f' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\translation\\store\\language.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1994159826c6d4173c8-45576346',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826c6d434f12_97263425',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826c6d434f12_97263425')) {function content_59826c6d434f12_97263425($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Translation
 * @subpackage Store
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware - Translation Manager Language Store
 *
 * Model for the translatable languages.
 */

//
Ext.define('Shopware.apps.Translation.store.Language',
/** @lends Ext.data.TreeStore# */
{
    /**
     * The parent class that this class extends
     * @string
     */
    extend: 'Ext.data.TreeStore',

    /**
     * Name of the Model associated with this store
     * @string
     */
    model: 'Shopware.apps.Translation.model.Language',

    /**
     * Remove previously existing child nodes before loading.
     * @boolean
     */
    clearOnLoad: false,

    /**
     * Indicates if the store's load method is automatically called after creation.
     * @boolean
     */
    autoLoad: false
});
//
<?php }} ?>