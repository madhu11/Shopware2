<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:21:01
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\translation\view\main\window.js" */ ?>
<?php /*%%SmartyHeaderCode:452859826c6d023842-35147168%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4c9617e10dd73e0d1e50dc72062d081e57ce2f1f' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\translation\\view\\main\\window.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '452859826c6d023842-35147168',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826c6d06a063_20373464',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826c6d06a063_20373464')) {function content_59826c6d06a063_20373464($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Translation
 * @subpackage View
 * @version    $Id$
 * @author shopware AG
 */

//

/**
 * Shopware UI - Translation Manager Main Window
 *
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Translation.view.main.Window',
/** @lends Enlight.app.Window# */
{
    extend: 'Enlight.app.Window',
    title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'window_title','default'=>'Translation','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','default'=>'Translation','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Translation<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'window_title','default'=>'Translation','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
    cls: Ext.baseCSSPrefix + 'translation-manager-window',
    alias: 'widget.translation-main-window',
    border: false,
    autoShow: true,
    layout: 'border',
    height: 600,
    width: 1000,

    /**
     * Initializes the component and builds up the main interface
     *
     * @public
     * @return void
     */
    initComponent: function() {
        var me = this;

        me.items = me.createItems();
        me.buttons = me.createActionButtons();

        me.callParent(arguments);
    },

    /**
     * Creates the items for the main window.
     *
     * @private
     * @return [array] generated items array
     */
    createItems: function() {
        var me = this;

        return [{
            xtype: 'translation-main-toolbar',
            dock: 'top',
            region: 'north'
        },{
            xtype: 'translation-main-navigation',
            treeStore: me.treeStore,
            region: 'west'
        }, {
            xtype: 'translation-main-form',
            translatableFields: me.translatableFields,
            region: 'center',
            autoScroll: true
        }];
    },

    /**
     * Creates the action buttons which will be
     * rendered to the main window footer
     *
     * @private
     * @return [array] generated buttons array
     */
    createActionButtons: function() {
        return [{
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button'/'cancel','default'=>'Cancel','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'cancel','default'=>'Cancel','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Cancel<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'cancel','default'=>'Cancel','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'secondary',
            action: 'translation-main-window-cancel'
        }, {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button'/'save_and_close','default'=>'Save and close','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'save_and_close','default'=>'Save and close','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save and close<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'save_and_close','default'=>'Save and close','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            action: 'translation-main-window-save-and-close'
        }, {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>'button'/'save','default'=>'Save translations','namespace'=>'backend/translation/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'save','default'=>'Save translations','namespace'=>'backend/translation/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save translations<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>'button'/'save','default'=>'Save translations','namespace'=>'backend/translation/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            cls: 'primary',
            action: 'translation-main-window-save'
        }];
    }
});
//
<?php }} ?>