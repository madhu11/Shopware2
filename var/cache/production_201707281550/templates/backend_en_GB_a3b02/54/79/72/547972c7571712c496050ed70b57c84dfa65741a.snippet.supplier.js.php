<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:12:53
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\supplier\model\supplier.js" */ ?>
<?php /*%%SmartyHeaderCode:920059826a851a3c59-25097200%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '547972c7571712c496050ed70b57c84dfa65741a' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\supplier\\model\\supplier.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '920059826a851a3c59-25097200',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826a851f5526_28376119',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826a851f5526_28376119')) {function content_59826a851f5526_28376119($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Supplier
 * @subpackage Model
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Model - Supplier
 *
 * Backend - Management for Suppliers. Create | Modify | Delete and Logo Management.
 * Standard supplier model
 */
//
Ext.define('Shopware.apps.Supplier.model.Supplier', {
    /**
     * Extends the standard ExtJS 4
     * @string
     */
    extend : 'Ext.data.Model',

    /**
     * The fields used for this model
     * @array
     */
    fields : [
        //
        { name : 'id', type : 'int' },
        { name : 'name', type : 'string' },
        { name : 'count', type : 'int' },
        { name : 'image', type : 'string' },
        { name : 'media-manager-selection', type: 'string' },
        { name : 'link', type : 'string'},
        { name : 'description', type : 'string' },
        { name : 'metaTitle', type : 'string' },
        { name : 'metaDescription', type : 'string' },
        { name : 'metaKeywords', type : 'string' },
        { name : 'articleCounter', type : 'int' }
    ],
    /**
     * If the name of the field is 'id' extjs assumes autmagical that
     * this field is an unique identifier.
     * @integer
     */
    idProperty : 'id',
    /**
     * Configure the data communication
     * @object
     */
    proxy : {
        type : 'ajax',
        api : {
            read : '<?php echo '/Projects/Generation4/Shopware/backend/supplier/getSuppliers';?>',
            create : '<?php echo '/Projects/Generation4/Shopware/backend/supplier/createSupplier';?>',
            update : '<?php echo '/Projects/Generation4/Shopware/backend/supplier/updateSupplier';?>',
            destroy : '<?php echo '/Projects/Generation4/Shopware/backend/supplier/deleteSupplier';?>'
        },
        reader : {
            type : 'json',
            root : 'data'
        }
    },
    /**
     * Rules to validate the input at the frontend side.
     * @array of objects
     */
    validations : [
        { field : 'name', type : 'length', min : 1 }
    ]
});
//
<?php }} ?>