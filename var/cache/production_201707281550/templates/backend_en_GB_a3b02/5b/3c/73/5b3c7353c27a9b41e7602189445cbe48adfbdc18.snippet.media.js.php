<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:14:16
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\media_manager\model\media.js" */ ?>
<?php /*%%SmartyHeaderCode:190359826ad879e9d9-81560633%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b3c7353c27a9b41e7602189445cbe48adfbdc18' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\media_manager\\model\\media.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '190359826ad879e9d9-81560633',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826ad88ab591_24241515',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826ad88ab591_24241515')) {function content_59826ad88ab591_24241515($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage Model
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.MediaManager.model.Media', {
    extend: 'Ext.data.Model',
    fields: [
        //
        'created',
        'description',
        'extension',
        'id',
        { name: 'name', sortType: 'asUCText' },
        'type',
        'path',
        'userId',
        'thumbnail',
        'width',
        'height',
        'albumId',
        'newAlbumID',
        'virtualPath',
        'timestamp'
    ],
    proxy: {
        type: 'ajax',
        api: {
            read: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/getAlbumMedia';?>',
            create: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/saveMedia';?>',
            update: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/saveMedia/targetField/media';?>',
            destroy: '<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/removeMedia';?>'
        },
        reader: {
            type: 'json',
            root: 'data',
            totalProperty: 'total'
        }
    }
});
//

<?php }} ?>