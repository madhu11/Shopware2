<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:12:28
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\base\attribute\field\Shopware.form.field.CategoryGrid.js" */ ?>
<?php /*%%SmartyHeaderCode:282085981f9ec2b1d59-24655070%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5f647aa3512be4a14ac8f3d1fa27058669544965' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\base\\attribute\\field\\Shopware.form.field.CategoryGrid.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '282085981f9ec2b1d59-24655070',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981f9ec316bf1_38007628',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981f9ec316bf1_38007628')) {function content_5981f9ec316bf1_38007628($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category    Shopware
 * @package     Base
 * @subpackage  Attribute
 * @version     $Id$
 * @author      shopware AG
 */

Ext.define('Shopware.form.field.CategoryGrid', {
    extend: 'Shopware.form.field.Grid',
    alias: 'widget.shopware-form-field-category-grid',
    mixins: ['Shopware.model.Helper'],

    createColumns: function() {
        var me = this;
        var activeColumn = { dataIndex: 'active', width: 30 };
        me.applyBooleanColumnConfig(activeColumn);

        return [
            me.createSortingColumn(),
            activeColumn,
            { dataIndex: 'name', flex: 1, renderer: me.nameRenderer, scope: me },
            me.createActionColumn()
        ];
    },

    createSearchField: function() {
        return Ext.create('Shopware.form.field.CategorySingleSelection', this.getComboConfig());
    },

    nameRenderer: function(value, meta, record) {
        var parents = Ext.clone(record.get('parents'));

        if (!parents || parents.length <= 0) {
            parents = [];
        }
        parents.push(value);
        return parents.join('>');
    }
});<?php }} ?>