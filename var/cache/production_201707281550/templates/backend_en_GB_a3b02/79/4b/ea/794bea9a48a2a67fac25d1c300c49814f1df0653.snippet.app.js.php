<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:14:15
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\media_manager\app.js" */ ?>
<?php /*%%SmartyHeaderCode:1108859826ad76f6377-25223577%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '794bea9a48a2a67fac25d1c300c49814f1df0653' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\media_manager\\app.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1108859826ad76f6377-25223577',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826ad776c2f6_13019231',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826ad776c2f6_13019231')) {function content_59826ad776c2f6_13019231($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage App
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware UI - Media Manager Bootstrapper
 *
 * This file bootstrapps the User Manager module. The module
 * handles the whole administration of the backend users.
 */

//
Ext.define('Shopware.apps.MediaManager', {

    /**
     * Extends from our special controller, which handles the
     * sub-application behavior and the event bus
     * @string
     */
    extend: 'Enlight.app.SubApplication',

    bulkLoad: true,

    /**
     * Sets the loading path for the sub-application.
     *
     * Note that you'll need a "loadAction" in your
     * controller (server-side)
     * @string
     */
    loadPath:'<?php echo '/Projects/Generation4/Shopware/backend/MediaManager/load';?>',

    /**
     * The name of the module. Used for internal purpose
     * @string
     */
    name: 'Shopware.apps.MediaManager',

    /**
     * Required controllers for module (subapplication)
     * @array
     */
    controllers: ['Main', 'Album', 'Media', 'Thumbnail'],

    /**
     * Requires models for sub-application
     * @array
     */
    models: ['Album', 'Media', 'Setting'],

    /**
     * Required views for this sub-application
     * @array
     */
    views: [
        'album.Add',
        'album.Setting',
        'album.Tree',
        'main.Selection',
        'main.Window',
        'media.View',
        'media.Grid',
        'thumbnail.Main',
        'batchMove.BatchMove',
        'replace.Window',
        'replace.Grid',
        'replace.Row',
        'replace.Upload',
        'replace.FileSelect'
    ],

    /**
     * Required stores for sub-application
     * @array
     */
    stores:['Album', 'Media'],

    /**
     * Returns the main application window for this is expected
     * by the Enlight.app.SubApplication class.
     * The class sets a new event listener on the "destroy" event of
     * the main application window to perform the destroying of the
     * whole sub application when the user closes the main application window.
     *
     * This method will be called when all dependencies are solved and
     * all member controllers, models, views and stores are initialized.
     *
     * @private
     * @return [object] mainWindow - the main application window based on Enlight.app.Window
     */
    launch: function() {
        var me = this,
            mainController = me.getController('Main');

        return mainController.mainWindow;
    }
});
//
<?php }} ?>