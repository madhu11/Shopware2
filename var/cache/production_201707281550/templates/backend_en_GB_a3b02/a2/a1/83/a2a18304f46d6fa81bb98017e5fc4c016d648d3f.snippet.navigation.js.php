<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:05
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\store\main\navigation.js" */ ?>
<?php /*%%SmartyHeaderCode:239735981fbb5849b11-02770735%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a2a18304f46d6fa81bb98017e5fc4c016d648d3f' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\store\\main\\navigation.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '239735981fbb5849b11-02770735',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbb588cb52_35446159',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbb588cb52_35446159')) {function content_5981fbb588cb52_35446159($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Config
 * @subpackage Config
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Config.store.main.Navigation', {
    extend:'Ext.data.TreeStore',
    model:'Shopware.apps.Config.model.main.Navigation',
    root: {
        id: 'root',
        expanded: true
    },
    proxy:{
        type:'ajax',
        url:'<?php echo '/Projects/Generation4/Shopware/backend/Config/getNavigation';?>',
        reader:{
            type:'json',
            root:'data'
        }
    }
});
//
<?php }} ?>