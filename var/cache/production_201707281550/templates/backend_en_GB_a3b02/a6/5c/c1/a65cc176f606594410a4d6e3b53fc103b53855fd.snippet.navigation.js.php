<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:03
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\model\main\navigation.js" */ ?>
<?php /*%%SmartyHeaderCode:63415981fbb36e11c9-87482898%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a65cc176f606594410a4d6e3b53fc103b53855fd' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\model\\main\\navigation.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '63415981fbb36e11c9-87482898',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbb3705233_94504094',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbb3705233_94504094')) {function content_5981fbb3705233_94504094($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Shopware_Config
 * @subpackage Config
 * @version    $Id$
 * @author shopware AG
 */

/**
 * todo@all: Documentation
 */
//
Ext.define('Shopware.apps.Config.model.main.Navigation', {
    extend: 'Ext.data.Model',
    fields: [
        //
        { name: 'id', type: 'int' },
        { name: 'text', convert: function(v, record) { return record.data.label; } },
        { name: 'leaf', convert: function(v, record) { return record.data.childrenCount <= 0; } },
        { name: 'label', type: 'string' },
        { name: 'childrenCount', type: 'int' }

        //{ name: 'loaded', type: 'boolean', defaultValue: false },
        //{ name: 'action' },
        //{ name: 'expanded', defaultValue: true },
        //{ name: 'children' }
    ]
});
//
<?php }} ?>