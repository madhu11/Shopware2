<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:14:16
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\media_manager\model\setting.js" */ ?>
<?php /*%%SmartyHeaderCode:1138859826ad8a1e689-24403648%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6cb4d0c92282841893323147d2e9174d2f714c2' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\media_manager\\model\\setting.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1138859826ad8a1e689-24403648',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826ad8a69074_00252665',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826ad8a69074_00252665')) {function content_59826ad8a69074_00252665($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    MediaManager
 * @subpackage Model
 * @version    $Id$
 * @author shopware AG
 */

//
Ext.define('Shopware.apps.MediaManager.model.Setting', {
    extend: 'Ext.data.Model',
    fields: [
        //
        {
            name: 'displayType',
            type: 'string'
        }, {
            name: 'itemsPerPage',
            type: 'number'
        }, {
            name: 'tableThumbnailSize',
            type: 'number'
        }, {
            name: 'gridThumbnailSize',
            type: 'number'
        }
    ]
});
//
<?php }} ?>