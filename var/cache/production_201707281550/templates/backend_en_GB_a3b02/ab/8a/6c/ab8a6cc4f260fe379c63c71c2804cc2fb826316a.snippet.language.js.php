<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:21:00
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\translation\model\language.js" */ ?>
<?php /*%%SmartyHeaderCode:1253159826c6cee9895-16014901%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ab8a6cc4f260fe379c63c71c2804cc2fb826316a' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\translation\\model\\language.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1253159826c6cee9895-16014901',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826c6cf15817_21686978',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826c6cf15817_21686978')) {function content_59826c6cf15817_21686978($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Translation
 * @subpackage Model
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware - Translation Manager Language Model
 *
 * Model for the translatable languages.
 */

//
Ext.define('Shopware.apps.Translation.model.Language',
/** @lends Ext.data.Model# */
{
    /**
     * The parent class that this class extends
     * @string
     */
    extend: 'Ext.data.Model',

    /**
     * The fields for this model.
     * @array
     */
    fields: [
        //
        { name: 'id', type: 'int' },
        { name: 'text', convert: function(v, record) { return record.data.name; } },
        { name: 'leaf', convert: function(v, record) { return record.data.childrenCount <= 0; } },
        { name: 'name', type: 'string' },
        { name: 'default', type: 'boolean' },
        { name: 'childrenCount', type: 'int' },
        { name: 'expanded', type: 'boolean', defaultValue: true, persist: false }
    ],

    /**
     * The proxy to use for this model.
     * @object
     */
    proxy: {
        type: 'ajax',
        url: '<?php echo '/Projects/Generation4/Shopware/backend/Translation/getLanguages';?>',

        /**
         * The Ext.data.reader.Reader to use to decode the server's response or data read from client.
         * @object
         */
        reader: {
            type: 'json',
            root: 'data',
            idProperty: 'id',
            totalProperty: 'total'
        }
    }
});
//
<?php }} ?>