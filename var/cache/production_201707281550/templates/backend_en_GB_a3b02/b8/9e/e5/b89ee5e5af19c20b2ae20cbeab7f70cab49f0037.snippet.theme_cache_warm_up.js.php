<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:12:35
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\index\model\theme_cache_warm_up.js" */ ?>
<?php /*%%SmartyHeaderCode:166055981f9f35065f6-59052489%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b89ee5e5af19c20b2ae20cbeab7f70cab49f0037' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\index\\model\\theme_cache_warm_up.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '166055981f9f35065f6-59052489',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981f9f351fc38_81251556',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981f9f351fc38_81251556')) {function content_5981f9f351fc38_81251556($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Theme cache warm up model
 *
 * Loads stores that use themes
 */
//
Ext.define('Shopware.apps.Index.model.ThemeCacheWarmUp', {
    extend: 'Shopware.apps.Base.model.Shop'
});
//
<?php }} ?>