<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:12:53
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\supplier\store\supplier.js" */ ?>
<?php /*%%SmartyHeaderCode:2628359826a85e4e173-04072714%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bfb3a869ff78105fcd93c45df02423955bc8267b' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\supplier\\store\\supplier.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2628359826a85e4e173-04072714',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826a85e69fa0_38937415',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826a85e69fa0_38937415')) {function content_59826a85e69fa0_38937415($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Supplier
 * @subpackage Store
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Store - Supplier
 *
 * Backend - Management for Suppliers. Create | Modify | Delete and Logo Management.
 * Default store which keeps the supplier
 */
//
Ext.define('Shopware.apps.Supplier.store.Supplier', {
    /**
     * Extend for the standard ExtJS 4
     * @string
     */
    extend : 'Ext.data.Store',
    /**
     * Store ID for easy access to this store
     *
     * @string
     */
    storeId: 'supplierStore',
    /**
     * Auto load the store after the component
     * is initialized
     * @boolean
     */
    autoLoad : false,
    /**
     * Amount of data loaded at once
     * @integer
     */
    pageSize : 30,
    /**
     * enables the remote filter system
     * @boolen
     */
    remoteFilter: true,
    /**
     * Enables the remote sorting system
     * @boolean
     */
    remoteSort : true,

    /**
     * Define the used model for this store
     * @string
     */
    model : 'Shopware.apps.Supplier.model.Supplier'
});
//
<?php }} ?>