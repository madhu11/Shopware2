<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:13:26
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\theme\view\detail\fields\color_picker.js" */ ?>
<?php /*%%SmartyHeaderCode:41885981fa26231f91-13809496%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cebcda8a6e22fa439ad4366e7fd04c44bb48cce9' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\theme\\view\\detail\\fields\\color_picker.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '41885981fa26231f91-13809496',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fa26280269_91655179',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fa26280269_91655179')) {function content_5981fa26280269_91655179($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Theme.view.detail.fields.ColorPicker', {
    extend: 'Shopware.form.field.ColorField',
    alias: 'widget.theme-color-picker',
    cls: 'theme-custom-field',
    margin: '5 0'
});

//

<?php }} ?>