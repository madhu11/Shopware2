<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:22
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\sorting\listing.js" */ ?>
<?php /*%%SmartyHeaderCode:52135981fbc6d80138-10667809%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd006710f156e2104ce6af80079c6611b4778bea7' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\sorting\\listing.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '52135981fbc6d80138-10667809',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc6dc39f8_94330982',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc6dc39f8_94330982')) {function content_5981fbc6dc39f8_94330982($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.sorting.Listing', {
    extend: 'Shopware.grid.Panel',
    alias: 'widget.config-custom-sorting-listing',

    configure: function() {
        return {
            deleteButton: false,
            editColumn: false,
            pagingbar: false,
            displayProgressOnSingleDelete: false,
            columns: {
                label: {
                    header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"sorting_label",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"sorting_label",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"sorting_label",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    sortable: false
                },
                active: {
                    header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"active",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Active<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    sortable: false
                },
                displayInCategories: {
                    header: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Display in all categories<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
                    sortable: false
                }
            }
        };
    },

    createColumns: function() {
        var me = this,
            columns = me.callParent(arguments);

        columns = Ext.Array.insert(columns, 0, [me.createSortingColumn()]);

        me.viewConfig = {
            plugins: {
                ptype: 'gridviewdragdrop',
                ddGroup: 'custom-sorting-drag-and-drop',
            },
            listeners: {
                'drop': Ext.bind(me.onDrop, me)
            }
        };

        return columns;
    },

    createSortingColumn: function() {
        var me = this;

        return {
            width: 24,
            hideable: false,
            renderer : me.renderSorthandleColumn
        };
    },

    renderSorthandleColumn: function (value, metadata) {
        return '<div style="cursor: n-resize;">&#009868;</div>';
    },

    createSelectionModel: function() {
        var me = this;

        return Ext.create('Ext.selection.RowModel', {
            listeners: {
                selectionchange: function (selModel, selection) {
                    return me.onSelectionChange(selModel, selection);
                }
            }
        });
    },

    onDrop: function(node, data, overModel, dropPosition, eOpts ) {
        var me = this,
            position = 0,
            model = data.records.shift();

        if (dropPosition == 'before') {
            position = overModel.get('position') - 1;
        } else {
            position = overModel.get('position') + 1;
        }

        Ext.Ajax.request({
            url: '<?php echo '/Projects/Generation4/Shopware/backend/customSorting/changePosition';?>',
            method: 'POST',
            params: {
                id: model.get('id'),
                position: position
            },
            success: function(operation, opts) {
                me.getStore().load();
            }
        });
    },

    onAddItem: function() {
        var me = this;
        me.sortingForm.setDisabled(false);
        me.sortingForm.loadRecord(
            Ext.create('Shopware.apps.Base.model.CustomSorting', {
                displayInCategories: true,
                active: true
            })
        );
        me.sortingForm.down('field[name=label]').focus();
    },

    onSelectionChange: function(selModel, selection) {
        var me = this;

        if (selection.length <= 0) {
            me.sortingForm.setDisabled(true);
            return;
        }
        me.onLoadSorting(selection[0]);
    },

    onLoadSorting: function(record) {
        var me = this;
        me.sortingForm.setDisabled(false);
        me.sortingForm.loadRecord(record);
    }
});

//
<?php }} ?>