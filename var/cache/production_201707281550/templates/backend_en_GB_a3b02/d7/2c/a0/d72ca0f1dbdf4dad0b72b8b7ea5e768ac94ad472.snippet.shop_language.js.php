<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:12:21
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\base\store\shop_language.js" */ ?>
<?php /*%%SmartyHeaderCode:163985981f9e51ddef1-95294269%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd72ca0f1dbdf4dad0b72b8b7ea5e768ac94ad472' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\base\\store\\shop_language.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '163985981f9e51ddef1-95294269',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981f9e51e5c48_85096862',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981f9e51e5c48_85096862')) {function content_5981f9e51e5c48_85096862($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Base
 * @subpackage Store
 * @version    $Id$
 * @author shopware AG
 */

/**
 * Shopware Store - Global Stores and Models
 *
 * Base store for both language and sub-shops
 */
Ext.define('Shopware.apps.Base.store.ShopLanguage', {
    extend: 'Shopware.apps.Base.store.Shop',
    alternateClassName: 'Shopware.store.ShopLanguage',
    storeId: 'base.ShopLanguage',
    filters: [ ]
}).create();

<?php }} ?>