<?php /* Smarty version Smarty-3.1.12, created on 2017-08-03 02:14:19
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\media_manager\view\replace\file_select.js" */ ?>
<?php /*%%SmartyHeaderCode:486259826adb40aba2-26000944%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd92c89f6f9b904875b4af8731e687cee406c933b' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\media_manager\\view\\replace\\file_select.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '486259826adb40aba2-26000944',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_59826adb472877_89778840',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59826adb472877_89778840')) {function content_59826adb472877_89778840($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 */

//
//
Ext.define('Shopware.apps.MediaManager.view.replace.FileSelect', {
    extend: 'Ext.form.field.File',

    anchor: '100%',
    margin: '10 0 20 0',
    name: 'images[]',
    labelStyle: 'font-weight: 700',
    labelWidth: 0,
    allowBlank: true,

    buttonConfig: {
        cls: 'secondary small',
        iconCls: 'sprite-inbox-image'
    },

    /**
     * init´s the component
     */
    initComponent: function() {
        var me = this;

        if (me.maxFileUpload > 1) {
            me.on('render', function() {
                me.fileInputEl.set({ multiple: true });
            });
        }

        me.callParent(arguments);
    },

    /**
     * @override
     */
    onFileChange: function(button, e, value) {
        this.duringFileSelect = true;

        var me = this,
            upload = me.fileInputEl.dom,
            files = upload.files,
            names = [];

        if (files) {
            for (var i = 0; i < files.length; i++)
                names.push(files[i].name);
            value = names.join(', ');
        }

        Ext.form.field.File.superclass.setValue.call(this, value);

        delete this.duringFileSelect;
    }
});
//<?php }} ?>