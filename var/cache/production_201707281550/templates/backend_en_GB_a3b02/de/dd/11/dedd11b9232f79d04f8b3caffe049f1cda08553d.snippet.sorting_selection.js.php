<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:23
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\sorting\sorting_selection.js" */ ?>
<?php /*%%SmartyHeaderCode:252515981fbc7195ab8-46882603%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dedd11b9232f79d04f8b3caffe049f1cda08553d' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\sorting\\sorting_selection.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '252515981fbc7195ab8-46882603',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc71d6c26_16505213',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc71d6c26_16505213')) {function content_5981fbc71d6c26_16505213($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

///

//

Ext.define('Shopware.apps.Config.view.custom_search.sorting.SortingSelection', {
    extend: 'Shopware.form.field.Grid',
    alias: 'widget.custom-search-sorting-selection',
    mixins: {
        factory: 'Shopware.attribute.SelectionFactory'
    },

    initComponent: function() {
        var me = this;

        me.sortingHandlers = me.sort(
            me.initSortings()
        );
        me.searchStore = me.createSearchStore();

        me.callParent(arguments);
    },

    sort: function(sortings) {
        return sortings.sort(function(a, b) {
            return a.getLabel().localeCompare(b.getLabel());
        });
    },

    initSortings: function() {
        return [
            Ext.create('Shopware.apps.Config.view.custom_search.sorting.classes.PriceSorting'),
            Ext.create('Shopware.apps.Config.view.custom_search.sorting.classes.ProductNameSorting'),
            Ext.create('Shopware.apps.Config.view.custom_search.sorting.classes.PopularitySorting'),
            Ext.create('Shopware.apps.Config.view.custom_search.sorting.classes.ReleaseDateSorting'),
            Ext.create('Shopware.apps.Config.view.custom_search.sorting.classes.SearchRankingSorting'),
            Ext.create('Shopware.apps.Config.view.custom_search.sorting.classes.ProductAttributeSorting')
        ];
    },

    initializeStore: function() {
        return this.store = Ext.create('Ext.data.Store', {
            fields: ['label', 'class', 'parameters']
        });
    },

    createSearchStore: function() {
        var me = this,
            data = [];

        Ext.each(me.sortingHandlers, function(item) {
            data.push({
                class: item,
                label: item.getLabel()
            });
        });

        return Ext.create('Ext.data.Store', {
            fields: ['label', 'class', 'parameters'],
            data: data,
            pageSize: null
        });
    },

    onSelect: function(combo, records) {
        var me = this;

        combo.clearValue();

        Ext.each(records, function(comboRecord) {
            var handler = comboRecord.get('class');

            handler.create(function(record) {
                if (me.recordExists(record.class)) {
                    Shopware.Notification.createGrowlMessage('', '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"singleton_error",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"singleton_error",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Each sort can be added only once<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"singleton_error",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
');
                    return;
                }
                me.store.add(record);
            });
        });
    },

    onBeforeSelect: function(combo, records) {
        return true;
    },

    recordExists: function(sortingClass) {
        var found = false;

        this.store.each(function(item, index) {
            if (item.get('class') == sortingClass) {
                found = item;
            }
        });
        return found;
    },

    setValue: function(value) {
        var me = this;

        me.store.removeAll();
        if (!value) {
            return;
        }

        try {
            var sorting = Ext.JSON.decode(value);
        } catch (e) {
            throw 'Sorting selection can not be decoded';
        }

        me.loadSortings(sorting);
    },

    loadSortings: function(sortings, callback) {
        var me = this,
            parameters,
            handler;

        for (var sortingClass in sortings) {
            parameters = sortings[sortingClass];
            handler = me.getHandler(sortingClass, parameters);

            if (handler) {
                handler.load(sortingClass, parameters, function(sorting) {
                    me.store.add(sorting);
                });
            } else {
                me.store.add({
                    label: sortingClass,
                    parameters: parameters
                });
            }
        }
    },

    getHandler: function(sortingClass, parameters) {
        var me = this,
            supportedHandler = null;

        Ext.each(me.sortingHandlers, function(handler) {
            if (handler.supports(sortingClass, parameters)) {
                supportedHandler = handler;
                return false;
            }
        });

        return supportedHandler;
    },

    getValue: function() {
        var me = this,
            recordData = { };

        me.store.each(function(item) {
            recordData[item.get('class')] = item.get('parameters');
        });

        if (recordData.length <= 0) {
            return null;
        }
        return Ext.JSON.encode(recordData);
    }
});

//
<?php }} ?>