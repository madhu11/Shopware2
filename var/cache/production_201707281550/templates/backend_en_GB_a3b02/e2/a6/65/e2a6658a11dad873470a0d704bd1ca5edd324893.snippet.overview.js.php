<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:21
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\overview.js" */ ?>
<?php /*%%SmartyHeaderCode:114145981fbc56f87d6-19753383%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e2a6658a11dad873470a0d704bd1ca5edd324893' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\overview.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '114145981fbc56f87d6-19753383',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc583e0a3_28309357',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc583e0a3_28309357')) {function content_5981fbc583e0a3_28309357($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.Overview', {
    extend: 'Shopware.apps.Config.view.base.Form',
    alias: 'widget.config-form-customsearch',
    flex: 1,

    getItems: function() {
        var me = this;
        return [
            me.createTab()
        ];
    },

    createTab: function() {
        var me = this;

        me.tabPanel = Ext.create('Ext.tab.Panel', {
            region: 'center',
            items: [
                me.createFacetTab(),
                me.createSortingTab()
            ]
        });
        return me.tabPanel;
    },

    createFacetTab: function() {
        var me = this;

        me.facetForm = Ext.create('Shopware.apps.Config.view.custom_search.facet.Detail', {
            width: 550,
            disabled: true,
            listeners: {
                'facet-saved': function() {
                    me.facetListing.getStore().load();
                }
            }
        });

        me.facetStore = Ext.create('Shopware.apps.Base.store.CustomFacet', {
            pageSize: 200
        }).load();

        me.facetListing = Ext.create('Shopware.apps.Config.view.custom_search.facet.Listing', {
            store: me.facetStore,
            flex: 2,
            subApp: me.subApp,
            facetForm: me.facetForm
        });

        return Ext.create('Ext.container.Container', {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"facet_tab",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"facet_tab",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filter<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"facet_tab",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [me.facetListing, me.facetForm]
        });
    },

    createSortingTab: function() {
        var me = this;

        me.sortingDetail = Ext.create('Shopware.apps.Config.view.custom_search.sorting.Detail', {
            record: Ext.create('Shopware.apps.Base.model.CustomSorting')
        });

        me.sortingForm = Ext.create('Ext.form.Panel', {
            items: [ me.sortingDetail ],
            width: 550,
            disabled: true,
            bodyPadding: '20 5',
            plugins: [{
                ptype: 'translation',
                translationMerge: true,
                translationType: 'custom_sorting'
            }],
            dockedItems: [{
                xtype: 'toolbar',
                dock: 'bottom',
                items: ['->', {
                    xtype: 'button',
                    cls: 'primary',
                    handler: Ext.bind(me.saveSorting, me),
                    text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
                }]
            }]
        });

        me.sortingListing = Ext.create('Shopware.apps.Config.view.custom_search.sorting.Listing', {
            store: Ext.create('Shopware.apps.Base.store.CustomSorting', { pageSize: 200 }).load(),
            flex: 2,
            sortingForm: me.sortingForm,
            sortingDetail: me.sortingDetail,
            subApp: me.subApp
        });

        return Ext.create('Ext.container.Container', {
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"sorting_tab",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"sorting_tab",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Sortings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"sorting_tab",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [me.sortingListing, me.sortingForm]
        });
    },

    saveSorting: function() {
        var me = this,
            record = me.sortingForm.getRecord();

        if (!me.sortingForm.getForm().isValid()) {
            return;
        }

        me.sortingForm.getForm().updateRecord(record);
        me.sortingForm.setDisabled(true);
        record.save({
            callback: function() {
                me.sortingListing.getStore().load();
            }
        });
    }
});

//
<?php }} ?>