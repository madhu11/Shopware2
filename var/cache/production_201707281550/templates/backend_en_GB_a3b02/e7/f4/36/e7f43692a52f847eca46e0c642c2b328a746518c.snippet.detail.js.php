<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:20:21
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\config\view\custom_search\facet\detail.js" */ ?>
<?php /*%%SmartyHeaderCode:219975981fbc5bc03d9-80104675%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e7f43692a52f847eca46e0c642c2b328a746518c' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\config\\view\\custom_search\\facet\\detail.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '219975981fbc5bc03d9-80104675',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fbc5bfb764_94746650',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fbc5bfb764_94746650')) {function content_5981fbc5bfb764_94746650($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

//

//

Ext.define('Shopware.apps.Config.view.custom_search.facet.Detail', {
    extend: 'Ext.form.Panel',
    alias: 'widget.config-custom-facet-detail',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    mixins: {
        factory: 'Shopware.attribute.SelectionFactory'
    },
    plugins: [{
        ptype: 'translation',
        translationMerge: true,
        translationType: 'custom_facet'
    }],
    bodyPadding: 15,
    autoScroll: true,

    initComponent: function() {
        var me = this;
        me.handlers = me.initHandlers();
        me.items = me.createItems();
        me.dockedItems = me.createDockedItems();
        me.callParent(arguments);
    },

    createItems: function() {
        var me = this;

        me.facetField = Ext.create('Shopware.apps.Config.view.custom_search.facet.Facet', {
            name: 'facet'
        });

        me.nameField = Ext.create('Ext.form.field.Text', {
            name: 'name',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"name",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"name",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Name<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"name",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            labelWidth: 150,
            allowBlank: false
        });

        me.activeField = Ext.create('Ext.form.field.Checkbox', {
            name: 'active',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"active",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Active<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            labelWidth: 150,
            helpText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"active_help_filter",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active_help_filter",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
If deactivated, this filter won\'t be displayed in the frontend. Custom assignments will remain, but won\'t be used.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"active_help_filter",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            inputValue: true,
            uncheckedValue: false
        });

        me.displayInCategoriesField = Ext.create('Ext.form.field.Checkbox', {
            name: 'displayInCategories',
            fieldLabel: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Display in all categories<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            labelWidth: 150,
            inputValue: true,
            helpText: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"display_in_categories_help_filter",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories_help_filter",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
If active, the filter can be used by default in categories, which don\'t have an individual filter configured. For this categories, you have to assign the filter manuelly.<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"display_in_categories_help_filter",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            uncheckedValue: false
        });

        return [{
            xtype: 'fieldset',
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"facet_settings",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"facet_settings",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
General settings<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"facet_settings",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [me.nameField, me.activeField, me.displayInCategoriesField]
        }, {
            xtype: 'fieldset',
            title: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"facet_configuration",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"facet_configuration",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Filter configuration<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"facet_configuration",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            items: [me.facetField]
        }];
    },

    createDockedItems: function() {
        var me = this;
        return [{
            xtype: 'toolbar',
            dock: 'bottom',
            items: me.createToolbarItems()
        }];
    },

    createToolbarItems: function() {
        var me = this;

        me.saveButton = Ext.create('Ext.button.Button', {
            cls: 'primary',
            handler: Ext.bind(me.saveFacet, me),
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Save<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"apply_button",'namespace'=>'backend/custom_search/translation'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
'
        });
        return ['->', me.saveButton];
    },

    saveFacet: function() {
        var me = this,
            record = me.getRecord();

        if (!me.getForm().isValid()) {
            return;
        }

        me.getForm().updateRecord(record);
        me.setDisabled(true);
        record.save({
            callback: function() {
                me.fireEvent('facet-saved');
            }
        });
    },

    loadFacet: function(record) {
        var me = this,
            facetDefinition,
            handler,
            facetClass;

        facetDefinition = Ext.JSON.decode(record.get('facet'));
        facetClass = Object.keys(facetDefinition);
        handler = me.getHandler(facetClass[0], facetDefinition[facetClass]);

        me.setDisabled(false);

        me.facetField.setHandler(handler);
        me.loadRecord(record);
    },

    getHandler: function(facetClass) {
        var me = this,
            supportedHandler = null;

        Ext.each(me.handlers, function(handler) {
            if (handler.getClass() == facetClass) {
                supportedHandler = handler;
                return false;
            }
        });
        return supportedHandler;
    },

    initHandlers: function() {
        return [
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.CategoryFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.ImmediateDeliveryFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.ManufacturerFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.PriceFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.PropertyFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.ShippingFreeFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.VoteAverageFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.ProductAttributeFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.CombinedConditionFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.WeightFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.LengthFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.HeightFacet'),
            Ext.create('Shopware.apps.Config.view.custom_search.facet.classes.WidthFacet')
        ];
    }
});

//
<?php }} ?>