<?php /* Smarty version Smarty-3.1.12, created on 2017-08-02 18:13:24
         compiled from "D:\xampp\htdocs\Projects\Generation4\Shopware\themes\Backend\ExtJs\backend\customer\view\customer_stream\listing.js" */ ?>
<?php /*%%SmartyHeaderCode:298845981fa24a2bd95-05751138%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f117c158d8e57a8642fc988e1b0b294c8428bb08' => 
    array (
      0 => 'D:\\xampp\\htdocs\\Projects\\Generation4\\Shopware\\themes\\Backend\\ExtJs\\backend\\customer\\view\\customer_stream\\listing.js',
      1 => 1501236364,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '298845981fa24a2bd95-05751138',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_5981fa24aa4581_78952390',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5981fa24aa4581_78952390')) {function content_5981fa24aa4581_78952390($_smarty_tpl) {?>/**
 * Shopware 5
 * Copyright (c) shopware AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Shopware" is a registered trademark of shopware AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 *
 * @category   Shopware
 * @package    Customer
 * @subpackage CustomerStream
 * @version    $Id$
 * @author shopware AG
 */

// 
// 
Ext.define('Shopware.apps.Customer.view.customer_stream.Listing', {
    extend: 'Shopware.grid.Panel',
    alias: 'widget.customer-stream-listing',
    cls: 'stream-listing',

    configure: function() {
        var me = this;

        return {
            pagingbar: false,
            toolbar: true,
            deleteButton: false,
            searchField: false,
            editColumn: false,
            displayProgressOnSingleDelete: false,

            /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('resource'=>'customerstream','privilege'=>'delete'),$_smarty_tpl);?>
<?php $_tmp1=ob_get_clean();?><?php if (!$_tmp1){?>*/
                deleteColumn: false,
            /*<?php }?>*/

            columns: {
                name: {
                    flex: 2,
                    renderer: me.nameRenderer
                },
                freezeUp: {
                    flex: 1,
                    renderer: me.freezeUpRenderer
                }
            }
        };
    },

    createAddButton: function() {
        var me = this,
            button = me.callParent(arguments);

        Ext.apply(button, {
            text: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"add_stream",'namespace'=>'backend/customer/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"add_stream",'namespace'=>'backend/customer/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Stream hinzufügen<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"add_stream",'namespace'=>'backend/customer/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            margin: 5,
            handler: function() {
                me.fireEvent('add-stream');
            }
        });
        return button;
    },

    createSelectionModel: function() {
        var me = this;

        me.selModel = Ext.create('Ext.selection.CheckboxModel', {
            mode: 'SINGLE',
            allowDeselect: true
        });
        return me.selModel;
    },

    createPlugins: function() {
        return [{
            ptype: 'grid-attributes',
            table: 's_customer_streams_attributes'
        }];
    },

    createColumns: function() {
        var me = this,
            columns = me.callParent(arguments);

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('resource'=>'customerstream','privilege'=>'save'),$_smarty_tpl);?>
<?php $_tmp2=ob_get_clean();?><?php if (!$_tmp2){?>*/
            return columns;
        /*<?php }?>*/

        columns.push({
            xtype: 'actioncolumn',
            width: 0,
            items: []
        });

        return columns;
    },

    createActionColumnItems: function() {
        var me = this, items = me.callParent(arguments);

        /*<?php ob_start();?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['acl_is_allowed'][0][0]->isAllowed(array('resource'=>'customerstream','privilege'=>'save'),$_smarty_tpl);?>
<?php $_tmp3=ob_get_clean();?><?php if (!$_tmp3){?>*/
            return items;
        /*<?php }?>*/

        items.push({
            iconCls: 'sprite-duplicate-article',
            action: 'duplicateStream',
            handler: function (view, rowIndex, colIndex, item, ops, record) {
                me.fireEvent('save-as-new-stream', record);
            },
            getClass: function (value, metadata, record) {
                if (record.get('static')) {
                    return 'x-hidden';
                }
            }
        });

        items.push({
            iconCls: 'sprite-arrow-circle-315',
            tooltip: '<?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"index_stream",'namespace'=>'backend/customer/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"index_stream",'namespace'=>'backend/customer/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Refresh stream customers<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"index_stream",'namespace'=>'backend/customer/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
',
            handler: function (view, rowIndex, colIndex, item, opts, record) {
                var node = me.getView().getNode(record),
                    el = Ext.get(node);
                el.addCls('rotate');

                me.fireEvent('save-stream-selection');

                me.fireEvent('index-stream', record, function () {
                    el.removeCls('rotate');
                    me.fireEvent('reset-progressbar');
                    me.getStore().load(function () {
                        me.fireEvent('restore-stream-selection');
                    });
                });
            },
            getClass: function (value, metadata, record) {
                if (record.get('freezeUp') || record.get('static')) {
                    return 'x-hidden';
                }
            }
        });

        return items;
    },

    freezeUpRenderer: function(value, meta, record) {
        var lockIcon = 'sprite-lock-unlock', freezeUp = '';

        if (value) {
            freezeUp = Ext.util.Format.date(value);
        }
        if (value || record.get('static')) {
            lockIcon = 'sprite-lock';
        }

        return '<span class="lock-icon ' + lockIcon + '">&nbsp;</span>' + freezeUp;
    },

    nameRenderer: function (value, meta, record) {
        var qtip = '<b>' + record.get('name') + '</b>';
        qtip += ' - ' + record.get('customer_count') + ' <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"customer_count_suffix",'namespace'=>'backend/customer/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"customer_count_suffix",'namespace'=>'backend/customer/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Customer(s)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"customer_count_suffix",'namespace'=>'backend/customer/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
';

        if (record.get('freezeUp')) {
            qtip += '<p><?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"freeze_up_label",'namespace'=>'backend/customer/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"freeze_up_label",'namespace'=>'backend/customer/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Until<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"freeze_up_label",'namespace'=>'backend/customer/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
: ' + Ext.util.Format.date(record.get('freezeUp')) + '</p>';
        }

        qtip += '<br><p>' + record.get('description') + '</p>';

        meta.tdAttr = 'data-qtip="' + qtip + '"';

        return '<span class="stream-name-column"><b>' + value + '</b> - ' + record.get('customer_count') + ' <?php $_smarty_tpl->smarty->_tag_stack[] = array('snippet', array('name'=>"customer_count_suffix",'namespace'=>'backend/customer/view/main')); $_block_repeat=true; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"customer_count_suffix",'namespace'=>'backend/customer/view/main'), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
Customer(s)<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo Enlight_Components_Snippet_Resource::compileSnippetBlock(array('name'=>"customer_count_suffix",'namespace'=>'backend/customer/view/main'), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</span>';
    }
});
// 
<?php }} ?>